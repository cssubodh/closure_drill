const cache=require('../cacheFunction.js');
const callBack=(...arg)=>{
    return arg;
}
let n=5;
const CACHE_CALL=cache(callBack);
CACHE_CALL(5,2,3);
CACHE_CALL(10);
CACHE_CALL(1);
CACHE_CALL(1);
console.log(CACHE_CALL(5));